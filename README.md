---
### ![goup.png](http://html-applications.bitbucket.io/images/goup.png) [Return to HTML Application page](http://html-applications.bitbucket.io) ###
---
## ![ado-find.png](http://html-applications.bitbucket.io/images/rtc.png)  [Remote Task Control](http://html-applications.bitbucket.io/remote-task-control/readme.html) ##
Is a tool to view and manage processes, services, drivers and installed software on a local and a remote computer.

Просмотр и управление процессами, сервисами, драйверами и установленным ПО как на локальном так и на удаленном компьютере.