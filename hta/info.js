function GetFilePath(path){
	// для имеющихся локальных файлов не лезем на удаленный комп
	if (oFSO.FileExists(path)) return path.toLowerCase();
	// для файлов на удаленном компе:
	if ((HOST != '.') && (idRemoteFilesInfo.checked)) {
		path = path.replace(/^(\w):/, '\\\\' + HOST + '\\$1$');
		if (oFSO.FileExists(path)) return path.toLowerCase();
	}
	return '';
}

// ExecutablePath -> Icon Path
function IconPath(path){
	// если имя присутствует в массиве aSystemIcons, возвращаем ''
	var exe_name = path.replace(/^.*\\/,'');
	var j = aSystemIcons.length; while (j--) {
		if (aSystemIcons[j] == exe_name) return '';
	}
	return GetFilePath(path);
}

// Замена дефолтовой иконки <IMG> на иконку <OBJECT> извлеченную из файла ядра процесса
function SetIcons(){
	var aIcons = document.getElementsByName('idIcon');
	var el, oIcon;
	var i = aIcons.length; while (i--) {
		el = aIcons[i];
		if (el.srcIcon) {
			oIcon = document.createElement('object');
			oIcon.setAttribute('classid', IconExtractClassId);
			oIcon.setAttribute('SourceFile', el.srcIcon);
			if (oIcon.getAttribute('ReturnCode') != 0) {
				el.parentNode.insertBefore(oIcon, el);
				el.parentNode.removeChild(el);
			} else {
				oIcon = null;
			}
		}
	}
}

// Получение внутренней информации из указанного файла
function GetFileVersionInfo(file_path){
	for (var i = 0; i < aInfoCache.length; i++) {
		if (aInfoCache[i].path == file_path) {
			return {
				description: aInfoCache[i].description,
				copyright: aInfoCache[i].copyright
			};
		}
	}
	file_path = GetFilePath(file_path);
	if (file_path) {
		var p = /^(.*?)([^\\]*)$/.exec(file_path);
		var exe, dscr, cpr;
		try {
			exe = oShellApp.NameSpace(p[1]).ParseName(p[2]);
			dscr = exe.ExtendedProperty("FileDescription");
			cpr = exe.ExtendedProperty("Copyright");
		} catch(e) {
			dscr = cpr = '<font color=red>&lt; inaccessible info &gt;</font>';
		}
		if (dscr || cpr) {
			aInfoCache[aInfoCache.length] = {
				path: file_path,
				description: dscr || '',
				copyright: cpr || ''
			};
			return {
				description: dscr || '',
				copyright: cpr || ''
			};
		}
	}
}

// Вычисляет время жизни (возраст) процесса
function GetAgeProcess(creationDate){
	// Преобразует время в секундах в строку вида hh:mm:ss
	function sec2hms(sec){
		var h = Math.floor(sec / (60 * 60)); if (h < 10) h = '0' + h;
		sec = sec % (60 * 60);
		var m = Math.floor(sec / 60); if (m < 10) m = '0' + m;
		sec = sec % 60;
		var s = sec; if (s < 10) s = '0' + s;
		return (h + ':' + m + ':' + s);
	}
	if (creationDate) {
		var current_date = new Date();
		var proc_date = new Date(creationDate.replace(/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2}).*/, '$1/$2/$3 $4:$5:$6'));
		var age = Math.ceil((current_date - proc_date) / 1000);
		return sec2hms(age);
	}
}

// Загрузка списка процессов в aList
function GetProcessesList(){
	aList = [];
	var colItems = oWMIService.ExecQuery ('SELECT * FROM Win32_Process');
	var enumItems = new Enumerator(colItems);
	var oItem, oRow, oOwner;
	for (; !enumItems.atEnd(); enumItems.moveNext()) {
		oItem = enumItems.item();
		oRow = {};
		oRow.Name = oItem.Name;
		oRow.pID = oItem.ProcessId;
		oRow.Size = oItem.WorkingSetSize / 1024;
		oRow.Age = GetAgeProcess(oItem.CreationDate);
		oRow.CommandLine = oItem.CommandLine;
		oRow.Parent = oItem.ParentProcessId;
		try {
			oOwner = oItem.ExecMethod_("GetOwner", null);
			if (oOwner.User) oRow.Owner = oOwner.Domain + '\\' + oOwner.User;
		} catch(e) {}
		oRow.ExecutablePath = oItem.ExecutablePath;
		var description = ''; var copyright = '';
		if (oItem.ExecutablePath) {
			oRow.Icon = IconPath(oItem.ExecutablePath);
			var ver_info = GetFileVersionInfo(oItem.ExecutablePath);
			if (ver_info) {
				description = ver_info.description;
				copyright = ver_info.copyright;
			}
		} else {
			oRow.Icon = '';
		}
		if (description == '') {
			if (oItem.Name == 'smss.exe') {
				description = 'Диспетчер сеанса Windows';
				copyright = '© Microsoft Corporation';
			}
			if (oItem.Name == 'audiodg.exe') {
				description = 'Изоляция графов аудиоустройств Windows';
				copyright = '© Microsoft Corporation';
			}
		}
		oRow.Description = description;
		oRow.Copyright = copyright;
		aList.push(oRow);
	}
}

// Заполнение списка процессов (если tree=true то перед иконкой процесса ставится отступ)
function FillProcessesList(tree){
	// Возвращает число, отформатированное с разделителем групп разрядов
	function format(num){
		num = num.toString();
		var s = '';
		var l = num.length;
		do {
			s = num.substring(l-3, l) + ' ' + s;
			l = l - 3;
		} while (l > 0);
		return s;
	}

	while (idList.rows.length) idList.deleteRow();
	var oItem, oRow;
	for (var i = 0; i < aList.length; i++) {
		oRow = idList.insertRow();
		oItem = aList[i];
		oRow.name = oItem.pID;
		with (oRow.insertCell()) {
			innerHTML = '<img id="idIcon" src="system.ico" srcIcon="' + oItem.Icon + '">&nbsp;' + oItem.Name;
			title = oItem.Name;
			style.paddingLeft = 4 + ((tree && oItem.level) ? oItem.level*16 : 0) + 'px';
		}
		with (oRow.insertCell()) {
			align = 'right';
			innerHTML = oItem.pID;
		}
		with (oRow.insertCell()) {
			align = 'right';
			innerHTML = format(oItem.Size) || '';
		}
		with (oRow.insertCell()) {
			align = 'center';
			innerHTML = oItem.Age || '';
		}
		with (oRow.insertCell()) {
			style.paddingLeft = '4px';
			innerHTML = oItem.CommandLine || '';
			title = oItem.CommandLine || '';
		}
		with (oRow.insertCell()) {
			style.paddingLeft = '4px';
			innerHTML = oItem.Owner || '';
			title = oItem.Owner || '';
		}

		with (oRow.insertCell()) {
			style.paddingLeft = '4px';
			innerHTML = oItem.Description;
			title = oItem.Description;
		}
		with (oRow.insertCell()) {
			style.paddingLeft = '4px';
			innerHTML = oItem.Copyright;
			title = oItem.Copyright;
		}
		if (/(Microsoft|Майкрософт)/i.test(oItem.Copyright)) oRow.style.color = 'blue';

		if (/^(System Idle Process|System|smss.exe|audiodg.exe)$/.test(oItem.Name)) oRow.style.color = 'gray';
		if (/^svchost.exe$/.test(oItem.Name)) oRow.style.color = '#FF0099';
		if (/^(cmd.exe|cscript.exe|wscript.exe|mshta.exe)$/.test(oItem.Name)) oRow.style.color = '#990000';
	}

	// if (document.body.offsetHeight > idList.offsetHeight) window.resizeTo(document.body.offsetWidth, idList.offsetHeight);
	SetTableBackground();
	if (IconExtractClassId) setTimeout('SetIcons()', 0);
}

// Загрузка списка сервисов/драйверов в aList
function GetServicesList(){
	aList = [];
	var colItems;
	if (MODE == 1) {
		colItems = oWMIService.ExecQuery ('SELECT * FROM Win32_Service');
	} else {
		colItems = oWMIService.ExecQuery ('SELECT * FROM Win32_SystemDriver');
	}
	var enumItems = new Enumerator(colItems);
	var oItem, exe_path;
	for (; !enumItems.atEnd(); enumItems.moveNext()) {
		oItem = enumItems.item();
		exe_path = oItem.PathName;
		exe_path = exe_path ? exe_path.replace(/^(\"(.*?)\".*|([A-Z]:\\[\w\\\.]+).*|\\\?\?\\(.*))$/i,'$2$3$4') : '';
		aList.push({
			Name: oItem.Name,
			Caption: oItem.Caption,
			Description: oItem.Description,
			Startup: oItem.StartMode,
			Path: oItem.PathName || '',
			ExecutablePath: exe_path,
			Icon: exe_path ? IconPath(exe_path) : '',
			Owner: oItem.StartName,
			Type: oItem.ServiceType,
			State: oItem.State
		});
	}
}

// Заполнение списка сервисов|драйверов
function FillServicesList(){
	while (idList.rows.length) idList.deleteRow();
	var oRow, oItem;
	for (var i = 0; i < aList.length; i++) {
		oRow = idList.insertRow();
		oItem = aList[i];
		oRow.name = oItem.Name;
		with (oRow.insertCell()) {
			style.paddingLeft = '4px';
			innerHTML = '<img id="idIcon" src="sd.ico"' + ((oItem.State == 'Running') ? '' : ' style="filter:Gray"') + ' srcIcon="' + oItem.Icon + '">&nbsp;' + oItem.Name;
			title = oItem.Name;
		}
		with (oRow.insertCell()) {
			style.paddingLeft = '4px';
			innerHTML = oItem.Caption;
			title = oItem.Description;
		}
		with (oRow.insertCell()) {
			align = 'center';
			innerHTML = oItem.State;
		}
		with (oRow.insertCell()) {
			align = 'center';
			innerHTML = oItem.Startup;
		}
		with (oRow.insertCell()) {
			style.paddingLeft = '4px';
			innerHTML = oItem.Path || '';
			title = oItem.Path || '';
		}
		if (MODE == 1) {
			with (oRow.insertCell()) {
				style.paddingLeft = '4px';
				innerHTML = oItem.Owner;
				title = oItem.Owner;
			}
		} else {
			with (oRow.insertCell()) {
				style.paddingLeft = '4px';
				innerHTML = oItem.Type;
				title = oItem.Type;
			}
		}

		if (oItem.Startup == 'Disabled') oRow.style.color = 'gray';
		if (oItem.State == 'Running') oRow.style.color = 'blue';
	}
	SetTableBackground();
	if ((MODE == 1) && IconExtractClassId) setTimeout('SetIcons()', 0);
}

// Загрузка списка установленного ПО в aList
function getSoftwareList(){
	function getSoftIcon(uninstallstring, pname){
		function getMSIIconPath(gui){
			var folder_name = oWshShell.ExpandEnvironmentStrings('%WINDIR%') + '\\Installer\\' + gui;
			if (oFSO.FolderExists(folder_name)) {
				var folder = oFSO.GetFolder(folder_name);
				var files = new Enumerator(folder.Files);
				var file;
				for (; !files.atEnd(); files.moveNext()){
					file = files.item();
					if (/\.ico|\.exe|icon|short/i.test(file.Name)) return file.Path;
				}
			}
		}

		function getProductIcon(product_name){
			// var oWMIRegistry = getRemoteRegistry(32);
			var key = "SOFTWARE\\Classes\\Installer\\Products";
			var a = GetRegistry(oWMIRegistry, "EnumKey", "HKLM", key);
			var rkey;
			for (var i = 0; i < a.length; ++i) {
				rkey = key + '\\' + a[i];
				if (GetRegistry(oWMIRegistry, "GetStringValue", "HKLM", rkey, "ProductName") == product_name) {
					return GetRegistry(oWMIRegistry, "GetStringValue", "HKLM", rkey, "ProductIcon") || '';
				}
			}
		}

		if (/^MsiExec\.exe .*?(\{[\w-]+\}).*/i.test(uninstallstring)) {
			return getProductIcon(pname);
			// return getMSIIconPath(RegExp.$1);
		} else if (/.*?"(.+)".*?/.test(uninstallstring)) {
			var iconfile = RegExp.$1;
			if (oFSO.FileExists(iconfile)) return iconfile;
		}
	}

	function getRegistrySoftware(regtype){
		oWMIRegistry = getRemoteRegistry(regtype);
		var a = GetRegistry(oWMIRegistry, "EnumKey", "HKLM", key);
		var rkey, name, ver, unstr, idate;
		for (var i = 0; i < a.length; ++i) {
			rkey = key + '\\' + a[i];
			name =  GetRegistry(oWMIRegistry, "GetStringValue", "HKLM", rkey, "DisplayName") || '';
			ver =   GetRegistry(oWMIRegistry, "GetStringValue", "HKLM", rkey, "DisplayVersion") || '';
			unstr = GetRegistry(oWMIRegistry, "GetStringValue", "HKLM", rkey, "UninstallString") || '';
			idate = GetRegistry(oWMIRegistry, "GetStringValue", "HKLM", rkey, "InstallDate") || '';
			if (/^(\d\d\d\d)(\d\d)(\d\d)$/.test(idate)) idate = RegExp.$3 + '.' + RegExp.$2 + '.' + RegExp.$1;
			if (name) {
				aList.push({
					Icon: getSoftIcon(unstr, name),
					Name: name,
					Version: ver,
					Uninstall: unstr,
					InstDate: idate,
					bit: regtype
				});
			}
		}
	}

	aList = [];
	var oWMIRegistry;
	var key = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall";
	getRegistrySoftware(32);
	getRegistrySoftware(64);
	aList.sort(function (a, b) {if(a.Name<b.Name) return -1; if(a.Name>b.Name) return 1; return 0;});
}

// Заполнение списка установленного ПО
function FillSoftwareList(){
	while (idList.rows.length) idList.deleteRow();
	var oRow, oItem;
	for (var i = 0; i < aList.length; i++) {
		oRow = idList.insertRow();
		oItem = aList[i];
		with (oRow.insertCell()) {
			style.paddingLeft = '4px';
			innerHTML = '<img id="idIcon" src="install.ico" srcIcon="' + oItem.Icon + '">&nbsp;' + oItem.bit;
			title = oItem.Name;
		}
		with (oRow.insertCell()) {
			style.paddingLeft = '4px';
			innerHTML = oItem.Name;
		}
		with (oRow.insertCell()) {
			innerHTML = oItem.Version;
		}
		with (oRow.insertCell()) {
			align = 'center';
			innerHTML = oItem.InstDate;
		}
		with (oRow.insertCell()) {
			innerHTML = oItem.Uninstall;
		}
	}
	SetTableBackground();
	if (IconExtractClassId) setTimeout('SetIcons()', 0);
}
