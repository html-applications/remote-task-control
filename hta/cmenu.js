/* ====================================================================
Показывает контекстное меню из пунктов Items

Объект ContextMenu имеет свойства:
 .icons - контекстное меню содержит иконки
 .checks - контекстное меню содержит чекбоксы
 .visible - меню в данный момент видимо (если .visible = true)
 
Объект ContextMenu имеет события:
 .onChange - при выборе пункта меню
 .onUnload - при закрытии меню
 
Каждый объект Item массива Items имеет свойства:
 .text - текст пункта меню (если .text = '-' то вставляется разделитель)
 .icon - ссылка на иконку 16х16 (или символ, задаваемый строкой вида 'O;normal 14px Wingdings;blue')
 .disabled - пункт меню неактивен (если .disabled = true)
 .checked - пункт меню отмечен чекбоксом (если .checked = true)
 .accent - пункт меню выделяется жирным шрифтом (если .accent = true)
 .action - функция, которая будет выполнена при выборе данного пункта меню
 .param0...2 - параметры, которые будут переданы в функцию
*/
function ContextMenu(Items) {
	this.Show = function () {
		var winContextMenu = window.createPopup();
		with (winContextMenu.document.createStyleSheet()){
			addRule('*','font:8pt MS Shell Dlg;');
			addRule('table','background-color:buttonface; font:menu; border:2px outset; border-collapse:collapse; cursor:default;');
			addRule('td','white-space:nowrap; padding:1px 2px;');
			addRule('.check','width:18px; font:18px Marlett; border-right:1px solid #2E81A9;');
			addRule('img','width:16px; height:16px;');
			addRule('div','border:1px inset; height:2px; overflow:hidden;');
		}
		winContextMenu.document.body.innerHTML = '<table id="idItemList"></table>';

		var Items = this.Items;
		for (var i = 0; i < Items.length; i++) {
			var NewRow = winContextMenu.document.all.idItemList.insertRow();
			if (Items[i].text == '-') {
				with (NewRow.insertCell()){
					colSpan = 1 + (this.checks || 0) + (this.icons || 0);
					innerHTML = '<div>&nbsp;</div>';
				}
			} else {
				NewRow.disabled = Items[i].disabled;
				if (this.checks) {
					with (NewRow.insertCell()) {
						className = 'check';
						if (Items[i].checked) innerHTML = 'a';
					}
				}
				if (this.icons) {
					with (NewRow.insertCell()) {
						style.height = '19px';
						if (Items[i].icon) {
							style.paddingLeft = '4px';
							if (/(\w);([\w\s]+);(\w+|[#0-9A-F]+)/i.test(Items[i].icon)) {
								innerHTML = '<span style="font:' + RegExp.$2 + ';color:' + RegExp.$3 + (Items[i].disabled ? ';disabled' : '')+'">' + RegExp.$1 + '</span>';
							} else {
								innerHTML = '<img src="' + Items[i].icon + '"' + (Items[i].disabled ? ' style=filter:Xray' : '')+'>';
							}
						}
					}
				}
				with (NewRow.insertCell()) {
					if (Items[i].accent) style.fontWeight = 'bolder';
					innerHTML = '&nbsp;' + Items[i].text + '&nbsp;';
				}
				// Обработка событий:
				NewRow.name = i;
				NewRow.onmouseover = function () {
					this.style.background = "activecaption";
					this.style.color = "#FFFFFF";
				};
				NewRow.onmouseout = function () {
					this.style.background = "";
					this.style.color = "";
				};
				var _f0 = this.onChange;
				NewRow.onclick = function () {
					_f0 (this.name);
					winContextMenu.hide();
				};
			}
		}

		winContextMenu.show(0, 0, 0, 0, document.body); // показываем меню (только для вычисления реальных размеров)
		var cm_width = winContextMenu.document.body.scrollWidth;
		var cm_height = winContextMenu.document.body.scrollHeight;
		var cm_y = (event.y+cm_height+window.screenTop > screen.availHeight) ? screen.availHeight-cm_height-window.screenTop : event.y;
		this.visible = true;
		winContextMenu.show(event.x, cm_y, cm_width, cm_height, window.document.body); // показываем меню заново, но уже вычисленного размера

		// Обработка события Unload
		var T = this;
		var _f1 = this.onUnload;
		winContextMenu.document.body.onunload = function(){
			T.visible = false;
			if (_f1) _f1();
		};
	}
}

// Контекстное меню для ряда таблицы
function ShowContextMenuRow(oCell){

	var separator = {text: '-'};

	if (oCell) {
		var oRow = oCell.parentElement;
		setRowBackground(oRow, '#FFFF80');
		var ItemShowDependencies = {
			text: 'Dependencies',
			icon: 'service.ico',
			action: ShowDependencies,
			param0: oRow.name,
			param1: event.clientX,
			param2: event.clientY + document.body.scrollTop
		};
		var ItemCopyToClipboard = {
			text: 'Copy to Clipboard',
			icon: 'copy.ico',
			action: CopyToClipboard,
			param0: oCell.innerText
		};
		var ItemShowFileProperties = {
			text: 'File Properties',
			icon: 'properties.ico',
			action: ShowFileProperties,
			param0: oRow.name
		};
		var ItemGoogleInfo = {
			text: 'Google',
			icon: 'web.ico',
			action: GoogleInfo,
			param0: oRow.firstChild.innerText
		};

		switch(MODE){
			case 0:
				oCM.Items = [
					ItemCopyToClipboard,
					ItemShowFileProperties,
					ItemGoogleInfo,
					{
						text: 'Kill Process',
						icon: 'delete.ico',
						action: KillProcess,
						param0: oRow.name
					}
				];
			break;
			case 1:
			case 2:
				oCM.Items = [
					ItemCopyToClipboard,
					ItemShowFileProperties,
					ItemGoogleInfo,
					ItemShowDependencies,
					separator,
					{
						text: 'Start',
						icon: 'start.ico',
						action: ServiceAction,
						param0: oRow.name,
						param1: 'start',
						disabled: oRow.style.color == 'blue'
					},
					{
						text: 'Stop',
						icon: 'stop.ico',
						action: ServiceAction,
						param0: oRow.name,
						param1: 'stop',
						disabled: oRow.style.color != 'blue'
					},
					separator,
					{
						text: 'Set Startup Auto',
						icon: 'A;normal 14px Arial Black;blue',
						action: ServiceAction,
						param0: oRow.name,
						param1: 'auto',
						disabled: oRow.cells[2].innerText == 'Auto'
					},
					{
						text: 'Set Startup Manual',
						icon: 'M;normal 14px Arial Black;green',
						action: ServiceAction,
						param0: oRow.name,
						param1: 'manual',
						disabled: oRow.cells[2].innerText == 'Manual'
					},
					{
						text: 'Set Startup Disabled',
						icon: 'D;normal 14px Arial Black;grey',
						action: ServiceAction,
						param0: oRow.name,
						param1: 'disabled',
						disabled: oRow.cells[2].innerText == 'Disabled'
					},
					separator,
					{
						text: 'Delete',
						icon: 'delete.ico',
						action: ServiceAction,
						param0: oRow.name,
						param1: 'delete'
					}
				];
			break;
			case 3:
				oCM.Items = [
					ItemCopyToClipboard,
					{
						text: 'Google',
						icon: 'web.ico',
						action: GoogleInfo,
						param0: oRow.cells[1].innerText
					},
					{
						text: 'Uninstall',
						icon: 'delete.ico',
						action: UninstallSoftware,
						param0: oRow.rowIndex
					}
				];
			break;
		}
	}

	oCM.onChange = function(item_num) {
		oCM.Items[item_num].action(oCM.Items[item_num].param0, oCM.Items[item_num].param1, oCM.Items[item_num].param2);
	};
	if (oCell) {
		oCM.onUnload = function() {
			setRowBackground(oRow, '');
		};
	}
	oCM.Show();
}
