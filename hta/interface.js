function SetModeTab() {
	idWait.style.display = '';
	(function() {
		setTimeout(function() {
			// ----------------------------------------------------------
					// Установка иконки приложения
					function SetAppIcon(icon) {
						var oHTA = document.getElementById('HTAPP');
						oHTA.removeNode();
						oHTA.icon = icon;
						document.getElementsByTagName('head')[0].appendChild(oHTA);
					}

					var table = document.getElementsByTagName('TABLE')[0];
					for (var i = 0; i < 4; i++) {
						table.rows[0].cells[i].className = (i==MODE) ? 'tab_on' : 'tab_off';
						table.rows[0].cells[i].children[0].style.filter = (i==MODE) ? '' : 'xray';
						table.rows[0].cells[i].onclick = (i==MODE) ? null : function () {
							var cell = event.srcElement;
							if (cell.tagName != 'TD') cell = cell.parentNode;
							MODE = cell.cellIndex;
							SetModeTab();
						};
					}

					var _title;
					if (HOST == '.'){
						_title = '\\\\' + oWshShell.ExpandEnvironmentStrings("%COMPUTERNAME%") + ' (this computer)';
					} else {
						_title = '\\\\' + HOST;
					}

					switch(MODE){
						case 0: //'process'
							SetAppIcon('process.ico');
							document.title = 'Processes on ' + _title;
							var headers = ['Name','ID','Size','Age','Command Line','Owner','Description','Copyright'];
							CreateHeader(headers);
							GetProcessesList();
							SortByTree();
							FillProcessesList(true);
							// RestoreColumnsWidth(',48,58,75');
							RestoreColumnsWidth(',40,50,60');
							break;
						case 1: // 'process'
							SetAppIcon('service.ico');
							document.title = 'Services on ' + _title;
							CreateHeader(['Name','Caption','State','Startup','Path','Owner']);
							GetServicesList();
							FillServicesList();
							RestoreColumnsWidth(',,52,52');
							break;
						case 2: // 'device'
							SetAppIcon('device.ico');
							document.title = 'Devices on ' + _title;
							CreateHeader(['Name','Caption','State','Startup','Path','Type']);
							GetServicesList();
							FillServicesList();
							RestoreColumnsWidth('93,,52,52,,90');
							break;
						case 3: // 'software'
							SetAppIcon('install.ico');
							document.title = 'Software on ' + _title;
							CreateHeader(['bit','Name','Version','Date','Uninstall']);
							getSoftwareList();
							FillSoftwareList();
							// RestoreColumnsWidth('40,355,92,70');
							RestoreColumnsWidth('40,,92,66');
							break;
					}
			// ----------------------------------------------------------
			idWait.style.display = 'none';
		}, 1000);
	}());


}

// Создание заголовка таблицы
function CreateHeader(aTitles){
	while (idHeader.cells.length) idHeader.deleteCell();
	var html;
	for (var i = 0; i < aTitles.length; i++) {
		html = '<table class="tHeader"><tr>';
		html += '<td id="idCaption">'+aTitles[i]+'</td>';
		html += '<td id="idSort" onmousedown="SortByColumn()">v</td>';
		html += '<td id="idRsz" onmousedown="ColumnResize_start()">&nbsp;</td>';
		html += '<tr></table>';
		idHeader.insertCell().innerHTML = html;
	}
}

// Подсветка/снятие ее с ряда таблицы
function setRowBackground(row, color){
	if (color) {  // Подсветка ряда
		var i = row.cells.length; while (i--) {
			row.cells[i].style.backgroundColor = color;
		}
	} else { // Снимаем подсветку с ряда (красим "зеброй")
		var i = row.cells.length; while (i--) {
			row.cells[i].style.backgroundColor = (i%2) ? '#E4EFF1' : '#FFFFFF';
		}
	}
}

// Окраска всей таблицы в вертикальную "зебру"
function SetTableBackground(){
	var i = idList.rows.length; while (i--) {
		setRowBackground(idList.rows[i], '');
	}
}

// Обработчик перемещения и клика мыши на ряду таблицы
function RowEventHandler(){
	if (!oCM.visible) {
		var row = event.srcElement.parentElement;
		if (row.nodeName == 'TR') {
			switch(event.type){
				case 'mouseup':   // Вызов контекстного меню
					if (event.button==2) ShowContextMenuRow(event.srcElement);
					break;
				case 'mouseover': // Подсветка текущего ряда
					setRowBackground(row, '#FFFF80');
					break;
				case 'mouseout': // Снимаем подсветку с текущего ряда
					setRowBackground(row, '');
					break;
			}
		}
	}
}

// ====================================================================
var startX = 0;                  // для изменения размеров ширины колонки
var startWidth = 0;              // для изменения размеров ширины колонки
var aColumnsWidth = [];          // массив для хранения размеров ширины колонок

// Старт изменения размера колонки (начали двигать зажатую мышь)
function ColumnResize_start() {
	startX = event.clientX;
	var cell_idx = event.srcElement.parentNode.parentNode.parentNode.parentNode.cellIndex;
	startWidth = idHeader.cells[cell_idx].offsetWidth;
	idHeader.cells[cell_idx].style.background = '#FFFFE1';
	document.onmousemove = function() {ColumnResize_move(cell_idx)};
	document.onmouseup = function() {ColumnResize_end(cell_idx)};
}

// Изменение размера колонки (двигаем зажатую мышь)
function ColumnResize_move(i){
	if (event.button <= 1) {
		var column_width = startWidth + event.clientX - startX;
		if (column_width > 10) {
			idHeader.cells[i].style.width = column_width;
			copyColumnWidth();
		}
		return false;
	}
}

// Конец изменения размера колонки (отпустили мышь)
function ColumnResize_end(i) {
	idHeader.cells[i].style.background = '';
	document.onmousemove=null;
	document.onmouseup=null;
	if (event.button <= 1) {
		aColumnsWidth[i] = idHeader.cells[i].offsetWidth;
	} else { // если нажата правая кнопка - сбрасываем ширину в auto
		aColumnsWidth[i] = '';
		idHeader.cells[i].style.width = '';
	}
	copyColumnWidth();
	Cookie(MODE, aColumnsWidth.join(','));
}

// Восстановление сохраненных размеров колонок
function RestoreColumnsWidth(default_set) {
	aColumnsWidth = [];
	var settings = Cookie(MODE) || default_set;
	aColumnsWidth = settings.split(',');
	var width;
	for (var i = 0; i < idHeader.cells.length; i++) {
		width = aColumnsWidth[i];
		if (width) {
			idHeader.cells[i].style.width = idList.rows[0].cells[i].style.width = width + 'px';
		}
	}
}

function copyColumnWidth(){
	for (var i = 0; i < idList.rows[0].cells.length; i++) {
		idList.rows[0].cells[i].style.width = idHeader.cells[i].style.width ? idHeader.cells[i].offsetWidth + 'px' : '';
	}
}

// ====================================================================
// Сохранение, восстановление, удаление настроек из cookie
function Cookie(name, value){
	function SetExpires(t) {
		var cookie_date = new Date();
		cookie_date.setTime(cookie_date.getTime() + t);
		return cookie_date.toGMTString();
	}
	if (value === undefined) {
		// Get Cookie
		if ((new RegExp('(?:^|; )'+name+'=([^;]*)')).test(document.cookie)) return unescape(RegExp.$1);
	} else {
		if (value === null) {
			// Delete Cookie
			document.cookie = name += "=; expires=" + SetExpires(-1);
		} else {
			// Set Cookie
			document.cookie = name + "=" + escape(value) + "; expires=" + SetExpires(1000*60*60*24*365);
		}
	}
}

// Сортировка списка по выбранной колонке
function SortByColumn(){
	// сравнение по алфавиту
	function compareAlph(a, b) {
		var x = String(a[ct] || '').toLowerCase();
		var y = String(b[ct] || '').toLowerCase();
		if (x < y) return -1;
		if (x > y) return 1;
		return 0;
	}
	// сравнение по числовому значению
	function compareNum(a, b) {
		return (Number(a[ct]) - Number(b[ct]));
	}
	// ------------------------------
	var src = event.srcElement;
	if (src.nodeName == 'TD') {
		var column = src.parentNode.parentNode.parentNode.parentNode.cellIndex;
		for (var i = 0; i < idSort.length; i++) {
			if (i == column) {
				idSort[i].innerText = (idSort[i].innerText == '5') ? '6' : '5';
			} else {
				idSort[i].innerText = 'v';
			}
		}
		var ct = src.previousSibling.innerText.replace(/\s+/, '');
		aList.sort(((ct == 'ID') || (ct == 'Size')) ? compareNum : compareAlph);
		if (idSort[column].innerText == '6') aList.reverse();
		switch(MODE){
			case 0:
				FillProcessesList();
				copyColumnWidth();
				break;
			case 1:
			case 2:
				FillServicesList();
				copyColumnWidth();
				break;
			case 3:
				FillSoftwareList();
				copyColumnWidth();
				break;
		}
	}
}

// Сортировка процессов по дереву
function SortByTree(){
	// если находит родительский процесс, то вставляет заданный ниже него
	function GetParent(j){
		var proc = aList[j]; aList.splice(j, 1); // сохраняем процесс и удаляем его из массива
		for (var i = 0; i < aList.length; i++) {
			if (aList[i].pID == proc.Parent){ // нашли родителя
				proc.level = (aList[i].level || 0) + 1;
				while ((aList[i+1] ? aList[i+1].level : 0) >= proc.level) i++; // если у родителя уже есть дети
				aList.splice(i+1, 0, proc); // вставляем процесс после имеющихся детей
				return;
			}
		}
		aList.splice(j, 0, proc); // вставляем процесс на прежнее место
	}

	for (var j = 0; j < aList.length; j++) {
		GetParent(j);
	}
}

window.onresize = function(){
	if (document.body.offsetHeight > 59) idContent.style.height = document.body.offsetHeight - 58 + 'px';
	idWait.style.width = document.body.offsetWidth - 10;
	idWait.style.height = document.body.offsetHeight - 10;
	if (window.screenLeft!=0) { // если не нажали кнопку "Развернуть"
		Cookie('win_size', document.body.clientWidth + ',' + document.body.clientHeight);
	}
};

// Обработка нажатий на клавиши
document.onkeydown = function() {
	switch(event.keyCode){
		case 112: // F1
			open("readme.html", "", "menubar=no,scrollbars=yes,status=no");
			break;
		case 116: // F5
			SetModeTab();
			break;
		case 27: // Esc
			self.close();
			break;
	}
}
