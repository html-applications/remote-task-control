// Копирование в буфер обмена
function CopyToClipboard(text){
	clipboardData.setData("Text", text);
	HHCtrl.TextPopup(text, "MS Sans Serif, 8, , plain", 4, 4, -1, -1);
}

// Поиск информации в Google
function GoogleInfo(text){
	oShellApp.Open('http://www.google.com/search?q=' + text);
}

// Диалоговое окно "Свойства файла"
function ShowFileProperties(id){
	function GetExecutablePath(pname){
		var i = aList.length; while (i--) {
			if (aList[i][pname] == id) return aList[i].ExecutablePath;
		}
	}
	var file_path = GetExecutablePath((MODE == 0) ? 'pID' : 'Name');
	if (HOST != '.') {
		file_path = file_path.replace(/^(\w):/, '\\\\' + HOST + '\\$1$');
	}
	if (oFSO.FileExists(file_path)) {
		oShellApp.NameSpace(0).ParseName(file_path).InvokeVerb("properties");
	} else {
		oWshShell.Popup('"' + file_path + '"\n    not exist!', 2, document.title, 16);
	}
}

// Убиение процесса
function KillProcess(pID){
	var colItems = oWMIService.ExecQuery ('SELECT * FROM Win32_Process WHERE ProcessId = ' + pID);
	var enumItems = new Enumerator(colItems);
	for ( ; !enumItems.atEnd(); enumItems.moveNext()) {
		var oProc = enumItems.item();
		if (oWshShell.Popup("Kill Process ID " + pID + " ?\n" + oProc.ExecutablePath, 0, document.title, 292) == 6) {
			KillProcessResume(oProc.Terminate(), oProc.Name);
		}
	}
}

// Обработка результатов выполнения команды KillProcess
function KillProcessResume (retcode, name) {
	switch(retcode){
		case 0: msg = 'Successful completion'; break;
		case 2: msg = 'Access denied'; break;
		case 3: msg = 'Insufficient privilege'; break;
		case 8: msg = 'Unknown failure'; break;
		case 9: msg = 'Path not found'; break;
		case 21: msg = 'Invalid parameter'; break;
		default: msg = 'Other'; break;
	}
	if (retcode==0) {
		SetModeTab();
	} else {
		oWshShell.Popup (msg, 0, "Kill Process " + name, 48);
	}
}

// Запуск, остановка, изменение режима старта, убиение сервиса
function ServiceAction(sName, action){
	var colItems;
	if (MODE == 1) {
		colItems = oWMIService.ExecQuery ('SELECT * FROM Win32_Service WHERE Name = "' + sName + '"');
	} else {
		colItems = oWMIService.ExecQuery ('SELECT * FROM Win32_SystemDriver WHERE Name = "' + sName + '"');
	}
	var enumItems = new Enumerator(colItems);
	for ( ; !enumItems.atEnd(); enumItems.moveNext()) {
		var oService = enumItems.item();
		switch(action){
			case 'start':
				ServiceActionResume (oService.StartService(), sName, 'Start');
				break;
			case 'stop':
				ServiceActionResume (oService.StopService(), sName, 'Stop');
				break;
			case 'auto':
				ServiceActionResume (oService.Change(null, null, null, null, "Automatic"), sName, 'Set Startup Auto');
				break;
			case 'manual':
				ServiceActionResume (oService.Change(null, null, null, null, "Manual"), sName, 'Set Startup Manual');
				break;
			case 'disabled':
				ServiceActionResume (oService.Change(null, null, null, null, "Disabled"), sName, 'Set Startup Disabled');
				break;
			case 'delete':
				if (oWshShell.Popup('Delete Service ' + sName + ' ?\n' + oService.Caption, 0, document.title, 292) == 6) {
					oService.StopService();
					ServiceActionResume (oService.Delete(), sName, 'Delete');
					break;
				}
		}
	}
}

// Обработка результатов выполнения команд ServiceAction
function ServiceActionResume (retcode, sName, action) {
	switch(retcode){
		case 0: msg = 'The request was accepted.'; break;
		case 1: msg = 'The request is not supported.'; break;
		case 2: msg = 'The user did not have the necessary access.'; break;
		case 3: msg = 'The service cannot be stopped because other services that are running are dependent on it.'; break;
		case 4: msg = 'The requested control code is not valid, or it is unacceptable to the service.'; break;
		case 5: msg = 'The requested control code cannot be sent to the service because the state of the service (Win32_BaseService.State property) is equal to 0, 1, or 2.'; break;
		case 6: msg = 'The service has not been started.'; break;
		case 7: msg = 'The service did not respond to the start request in a timely fashion.'; break;
		case 8: msg = 'Unknown failure when starting the service.'; break;
		case 9: msg = 'The directory path to the service executable file was not found.'; break;
		case 10: msg = 'The service is already running.'; break;
		case 11: msg = 'The database to add a new service is locked.'; break;
		case 12: msg = 'A dependency this service relies on has been removed from the system.'; break;
		case 13: msg = 'The service failed to find the service needed from a dependent service.'; break;
		case 14: msg = 'The service has been disabled from the system.'; break;
		case 15: msg = 'The service does not have the correct authentication to run on the system.'; break;
		case 16: msg = 'This service is being removed from the system.'; break;
		case 17: msg = 'The service has no execution thread.'; break;
		case 18: msg = 'The service has circular dependencies when it starts.'; break;
		case 19: msg = 'A service is running under the same name.'; break;
		case 20: msg = 'The service name has invalid characters.'; break;
		case 21: msg = 'Invalid parameters have been passed to the service.'; break;
		case 22: msg = 'The account under which this service runs is either invalid or lacks the permissions to run the service.'; break;
		case 23: msg = 'The service exists in the database of services available from the system.'; break;
		case 24: msg = 'The service is currently paused in the system.'; break;
		default: msg = 'Other'; break;
	}
	if (retcode==0) {
		SetModeTab();
	} else {
		oWshShell.Popup (msg, 0, 'Service ' + sName + ' ' + action, 48);
	}
}

// Показ зависимостей сервиса
function ShowDependencies(id, x, y){
	var colItems, enumItems, oItem;
	colItems = oWMIService.ExecQuery ("Associators of {Win32_Service.Name='"+id+"'} Where AssocClass=Win32_DependentService Role=Dependent");
	enumItems = new Enumerator(colItems); var html1 = '';
	for (; !enumItems.atEnd(); enumItems.moveNext()) {
		oItem = enumItems.item();
		html1 += '<tr><td>' + oItem.Name + '<td>' + oItem.Caption;
	}
	colItems = oWMIService.ExecQuery ("Associators of {Win32_Service.Name='"+id+"'} Where AssocClass=Win32_DependentService Role=Antecedent");
	enumItems = new Enumerator(colItems); var html2 = '';
	for (; !enumItems.atEnd(); enumItems.moveNext()) {
		oItem = enumItems.item();
		html2 += '<tr><td>' + oItem.Name + '<td>' + oItem.Caption;
	}
	var html0 = '<table><tr style="background-color:#ABCBD3;"><td style="color:blue; font:12px Marlett; padding:0 1px;">3<td style="text-align:right; color:red; font:10px Marlett; padding:0 1px;">r';
	html1 = '<tr><th colspan=2>This service depends on the following system components:' + (html1 || '<tr><td>&lt;No Dependencies&gt;');
	html2 = '<tr><th colspan=2>The following system components depend on this service:'  + (html2 || '<tr><td>&lt;No Dependencies&gt;') + '</table>';
	var oInfo = document.createElement("DIV");
	oInfo.innerHTML = html0 + html1 + html2;
	oInfo.className = 'dependencies';
	oInfo.onclick = function(){this.parentNode.removeChild(this);};
	oInfo.style.left = x + 'px';
	oInfo.style.top = y + 'px';
	document.body.appendChild(oInfo);
}

function UninstallSoftware(row_idx){
	function runRemote(cmdline){
		function fDescript(objOutParams) {
			switch(objOutParams.ReturnValue){
				case 0:  return "Command successfully completed. " + "Process ID: " + objOutParams.ProcessId;
				case 2:  return "Err 2: Access Denied";
				case 3:  return "Err 3: INsufficient Privilege";
				case 8:  return "Err 8: Unknown Failure";
				case 9:  return "Err 9: Path not found"; 
				case 21: return "Err 21: Invalid Parameter";
			}
		}
		var oInParam = oWMIService.Get("Win32_Process").Methods_("Create").inParameters.SpawnInstance_();
		oInParam.Properties_.Item("CommandLine").Value = cmdline;
		return fDescript(oWMIService.ExecMethod("Win32_Process", "Create", oInParam));
	}

	var row = idList.rows[row_idx-1];
	var app_name = row.cells[1].innerText;
	if (oWshShell.Popup('Are you sure you want to uninstall :\n' + app_name, 0, document.title, 36) == 6) {
		var uninstallstring = row.cells[4].innerText;
		if (/^MsiExec\.exe .*?(\{[\w-]+\}).*/i.test(uninstallstring)) {
			uninstallstring = 'MsiExec.exe /X' + RegExp.$1 + ' /QN REBOOT=R';
		} else if (!/ \/silent/i.test(uninstallstring)) {
			uninstallstring += ' /silent';
		}
		var ret_msg = runRemote(uninstallstring);
		oWshShell.Popup (ret_msg, 0, "Uninstall Software\n" + app_name, 48);
	}
}

// Перезапуск с повышенными правами
function RunAsAdmin(){
	oShellApp.ShellExecute ('mshta', HTAPP.commandLine, "", "runas", 1);
	self.close();
}

// Подключение к хосту
function ConnectTo(comp) {
	if (getWMIService(comp)) {
		HOST = comp;
		SetModeTab();
	}
}

// Изменение хоста подключения
function ChangeHost(){
	var new_host = prompt('Please enter the name or ip address of the remote computer', 'RemoteComputer');
	if (new_host) {
		ConnectTo(new_host);
	}
}
