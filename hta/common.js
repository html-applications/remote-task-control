// ��������� ��������� ������ ������� ����������
function ParseCmdLine(cmd){
	var oCmdLine = {};
	oCmdLine.commands = [];
	oCmdLine.params = [];
	var Arr_cmd = cmd.match(/[^ "]+|"[^"]+"/g);
	for (i=1; i<Arr_cmd.length; i++) {
		var x = Arr_cmd[i].replace(/"/g,'');
		if (/^(-|--|\/)(\w+)$/.test(x)) {
			oCmdLine.params.push(RegExp.$2);
		} else {
			oCmdLine.commands.push(x);
		}
	}
	return oCmdLine;
}

// ����������� IconExtract ActiveX
function InitAxIconExtract() {
	function GetCLSID() {
		IconExtractClassId = '';
		try {
			var clsid = oWshShell.RegRead('HKCR\\AxControl.IconExtract\\Clsid\\');
			if (clsid) IconExtractClassId = 'clsid:' + clsid.replace(/[{}]/g, '');
		} catch(e) {}
	}

	GetCLSID();
	if (IconExtractClassId) {
		var oIcon = document.createElement('object');
		oIcon.setAttribute('classid', IconExtractClassId);
		if (oIcon.ReturnCode > -1) return;
		IconExtractClassId = '';
	}

	try {
		oWshShell.RegRead("HKEY_USERS\\S-1-5-19\\Environment\\TEMP");
	} catch(e) {
		RunAsAdmin();
	}

	var msvbvm = oWshShell.ExpandEnvironmentStrings("%WINDIR%\\system32\\msvbvm60.dll");
	if (!oFSO.FileExists(msvbvm)){
		oWshShell.Popup("File " + msvbvm + " does not exist", 4, "Error IconExtract registration", 16);
		return;
	}

	var script_path = unescape(document.URL).replace(/^file:\/\/(.+?)[^\\]+$/, '$1');
	var fileIconExtract = script_path + "IconExtract.ocx";
	var system = oWshShell.ExpandEnvironmentStrings("%WINDIR%\\system32\\");
	try {
		oFSO.CopyFile(fileIconExtract, system, true);
		var ret = oWshShell.Run('regsvr32.exe /s "' + system + 'IconExtract.ocx"', 1, true);
		if (ret == 0) {
			GetCLSID(); return;
		}
	} catch(e) {}
}

// ����������� � ���������� ����������
function getWMIService(comp){
	try {
		var cs = oSWbemLocator.ConnectServer(comp, "root\\CIMV2");
		oWMIService = cs;
		return true;
	} catch(e) {
		var error = e.number & 0xFFFF;
		if (error == 5) {
			var ret = showModalDialog('logon.html', {host:comp, fCookie:Cookie}, 'resizable:no; scroll:no; status:no; help:no; dialogWidth:422px; dialogHeight:252px;');
			if (ret) {
				if (ret.error) {
					oWshShell.Popup(ret.error, 2, "Connect with remembed credentials", 16);
				} else {
					oWMIService = ret.CIM;
					return true;
				}
			}
		} else {
			oWshShell.Popup("Error connecting to host " + comp + "\n" + error + " : " + e.description, 2, "Connect with current credentials", 16);
		}
	}
}

// ������ �������
function GetRegistry(oWMIReg, method, hive, key, value){
	var readMethod = oWMIReg.Methods_.Item(method);
	var inputParams = readMethod.InParameters.SpawnInstance_();
	switch(hive){
		case "HKCR": inputParams.hDefKey = 0x80000000; break;
		case "HKCU": inputParams.hDefKey = 0x80000001; break;
		case "HKLM": inputParams.hDefKey = 0x80000002; break;
		case "HKU":  inputParams.hDefKey = 0x80000003; break;
		case "HKCC": inputParams.hDefKey = 0x80000005;
	}
	inputParams.sSubKeyName = key;
	if (value) inputParams.sValueName = value;
	var outputParams = oWMIReg.ExecMethod_(readMethod.Name, inputParams);
	if (outputParams.ReturnValue === 0) {
		switch(method){
			case "GetStringValue":
			case "GetExpandedStringValue":
				return outputParams.sValue;
			case "GetDWORDValue":
				return outputParams.uValue;
			case "GetMultiStringValue":
				return VBArray(outputParams.sValue).toArray();
			case "GetBinaryValue":
				return VBArray(outputParams.uValue).toArray();
			case "EnumKey":
			case "EnumValues":
				return (outputParams.sNames != null) ? outputParams.sNames.toArray() : null;
		}
	}
}

function getRemoteRegistry(regtype){
	var oCtx = new ActiveXObject("WbemScripting.SWbemNamedValueSet");
	oCtx.Add ("__ProviderArchitecture", regtype);
	return oSWbemLocator.ConnectServer(HOST, "root\\DEFAULT","", "", null, null, null, oCtx).Get("StdRegProv");
}

// �������� ������ ������ Date.toLocaleFormat(format_string)
Date.prototype.toLocaleFormat = function(format) {
	var f = {
		Y : this.getFullYear(), 
		y : this.getFullYear()-(this.getFullYear()>=2e3?2e3:1900), 
		m : this.getMonth() + 1,
		d : this.getDate(),
		H : this.getHours(),
		M : this.getMinutes(),
		S : this.getSeconds()
	}, k;
	for(k in f) 
		format = format.replace('%' + k, f[k] < 10 ? "0" + f[k] : f[k]);
	return format;
}

