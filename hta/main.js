function CheckElevated() {
	try {
		oWshShell.RegRead("HKEY_USERS\\S-1-5-19\\Environment\\TEMP");
		idAdmin.checked = true;
		idAdmin.disabled = true;
		idAdmin.parentNode.style.color = '#174155';
	} catch(e) {
		idAdmin.checked = false;
	}
}

// Действия при загрузке приложения
function Window_OnLoad(){
	CheckElevated();
	InitAxIconExtract();

	var oCmdLine = ParseCmdLine(HTAPP.commandLine);
	var comp = oCmdLine.commands[0];
	comp = (comp) ? comp.replace(/^\\\\/, '') : '.';
	for (var i = 0; i < oCmdLine.params.length; i++) {
		switch(oCmdLine.params[i].toLowerCase().charAt(0)){
			case 's': // services
				MODE = 1; break;
			case 'd': // devices
				MODE = 2; break;
			case 'i': // install_software
				MODE = 3; break;
			case 'n':
				idRemoteFilesInfo.checked = false; break;
		}
	}

	var win_size = Cookie('win_size');
	if (win_size) {
		win_size = win_size.split(',');
		window.resizeTo(win_size[0], win_size[1]);
	}

	ConnectTo(comp);
}

