@echo off
CD /D "%~dp0"

rem https://www.7-zip.org/
set Sz="c:\Program Files (x86)\7-Zip\7z.exe"
if not exist %Sz% set Sz="..\..\7Zip\7z.exe"
if not exist %Sz% echo 7zip not exist! & pause & exit /b

for /f "tokens=2 delims==" %%a in ('findstr "version=" ..\hta\rtc.hta') do set ver=%%~a
echo rtc.hta version %ver%
rem http://blog.dixo.net/downloads/stampver/
stampver.exe -f"%ver%.0" -p"%ver%.0" rtc.sfx

pushd "..\hta"
	%Sz% a %~dp0archive.7z .\
popd

if exist ..\rtc.exe del /q ..\rtc.exe

copy /b rtc.sfx + config.txt + archive.7z ..\rtc.exe

del /q archive.7z

pause